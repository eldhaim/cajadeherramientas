package co.com.angelroman.misiontic.cajadeherramientas.model;

import java.util.ArrayList;
import java.util.List;

import co.com.angelroman.misiontic.cajadeherramientas.mvp.MainMVP;
import co.com.angelroman.misiontic.cajadeherramientas.view.AppDataBase;
import co.com.angelroman.misiontic.cajadeherramientas.view.ListaTareas;
import co.com.angelroman.misiontic.cajadeherramientas.view.dto.TaskItem;
import co.com.angelroman.misiontic.cajadeherramientas.view.dto.TaskState;


public class MainInteractor implements MainMVP.Model {

    private List<TaskItem> tempItems;

    public MainInteractor(AppDataBase db, String usuario) {
        tempItems = new ArrayList<>();
        List<ListaTareas> user = db.listaTareasDao().findByName(usuario);
        ArrayList<ListaTareas> listaUser = verificaUsuario(user);
        for(int i = 0; i<listaUser.size();i++){
            TaskItem state = new TaskItem("tarea","date",TaskState.PENDING);
            if(listaUser.get(i).getEstado().equals("N")){
                state.setState(TaskState.PENDING);
                System.out.println("MAININTERACTOR ENTRA POR NO");
                tempItems.add(new TaskItem(listaUser.get(i).nom_tarea, listaUser.get(i).fecha,state.getState()));
            }else if(listaUser.get(i).getEstado().equals("S") ){
                state.setState(TaskState.DONE);
                System.out.println("MAININTERACTOR ENTRA POR SI");
                tempItems.add(new TaskItem(listaUser.get(i).nom_tarea, listaUser.get(i).fecha,state.getState()));
            }else{
                System.out.println("MAININTERACTOR PROBLEMA");
            }
        }
    }

    @Override
    public List<TaskItem> getTasks() {
        return new ArrayList<>(tempItems);
    }

    @Override
    public void saveTask(TaskItem task) {
        tempItems.add(task);
    }

    @Override
    public void updateTask(TaskItem item) {

    }

    @Override
    public void deleteTask(TaskItem task) {

    }
    private ArrayList<ListaTareas> verificaUsuario(List<ListaTareas> lista){
        ArrayList<ListaTareas> listaArrayUsuario = new ArrayList<>();
        for(int i = 0; i<lista.size();i++){
            listaArrayUsuario.add(i, lista.get(i));
        }
        return listaArrayUsuario;
    }
}
