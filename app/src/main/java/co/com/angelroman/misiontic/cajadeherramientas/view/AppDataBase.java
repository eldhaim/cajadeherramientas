package co.com.angelroman.misiontic.cajadeherramientas.view;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Usuarios.class, ListaTareas.class}, version = 2)
public abstract class AppDataBase extends RoomDatabase {
    public abstract  UsuariosDao ususariosDao();
    public abstract  ListaTareasDao listaTareasDao();

}
