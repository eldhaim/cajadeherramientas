package co.com.angelroman.misiontic.cajadeherramientas.view;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ListaTareasDao {

    @Query("SELECT * FROM tarea")
    List<ListaTareas> getAll();

    @Query("SELECT * FROM tarea WHERE usuario = :usuario")
    List<ListaTareas> findByName(String usuario);
    @Query("SELECT * FROM tarea WHERE usuario = :usuario AND nom_tarea = :tarea")
    List<ListaTareas> findByNameTask(String usuario, String tarea);

    @Query("DELETE FROM tarea WHERE id_tarea = :id")
    int deleteTask(int id);

    @Insert
    Long insert(ListaTareas tarea);

    @Query("UPDATE tarea set estado = :state WHERE id_tarea = :id")
    int updateTask(String state, int id);


}
