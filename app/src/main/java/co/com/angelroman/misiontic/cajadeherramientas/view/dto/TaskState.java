package co.com.angelroman.misiontic.cajadeherramientas.view.dto;

public enum TaskState {
    PENDING,
    DONE
}
