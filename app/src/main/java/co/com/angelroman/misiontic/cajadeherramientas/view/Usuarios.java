package co.com.angelroman.misiontic.cajadeherramientas.view;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(primaryKeys = {"usuario"})
public class Usuarios {
    @NonNull
    @ColumnInfo (name = "usuario")
    public String usuario;
    @ColumnInfo(name = "password")
    public String password;

    public Usuarios(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}