package co.com.angelroman.misiontic.cajadeherramientas.view;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UsuariosDao {
    @Query("SELECT * FROM Usuarios")
    List<Usuarios> getAll();

    @Query("SELECT * FROM Usuarios WHERE usuario = :usuario")
    List<Usuarios> findByName(String usuario);

    @Insert
    Long insert(Usuarios usuario);
}
