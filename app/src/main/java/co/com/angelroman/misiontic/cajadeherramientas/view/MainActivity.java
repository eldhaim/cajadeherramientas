package co.com.angelroman.misiontic.cajadeherramientas.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.List;


import java.util.ArrayList;

import co.com.angelroman.misiontic.cajadeherramientas.R;
import java.util.regex.*;
import java.util.*;

//RECORDAR BOTON ATRAS VIDEO DE INSERT DE REGISTROS MINUTO 22!!!!!!!!!!
public class MainActivity extends AppCompatActivity {

    EditText et_mail, et_pass;
    Button Btnlogin;
    ArrayList<String> listaArrayUsuario = new ArrayList<>();
    String usuario_1;
    String contrasena;
    public static String usuario_tarea = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(MainActivity.this,"BIENVENIDO", Toast.LENGTH_LONG).show();

        et_mail = findViewById(R.id.et_mail);
        et_pass = findViewById(R.id.et_pass);
        Btnlogin = findViewById(R.id.Btnlogin);

        et_mail.setText("");
        et_pass.setText("");

        Btnlogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                AppDataBase db = Room.databaseBuilder(MainActivity.this,
                        AppDataBase.class, "CajaDeherramientas2").allowMainThreadQueries().build();
                usuario_1 = et_mail.getText().toString();
                contrasena = et_pass.getText().toString();
                //QUERY
                List<Usuarios> lista = db.ususariosDao().findByName(usuario_1);
                listaArrayUsuario = verificaUsuario(lista,usuario_1);
                if(usuario_1.isEmpty() || contrasena.isEmpty()){
                    Toast.makeText(MainActivity.this,"DEBE INGRESAR SU USUARIO Y CONTRASEÑA",
                            Toast.LENGTH_LONG).show();
                }else if(validaCorreo(usuario_1) == false){
                    Toast.makeText(MainActivity.this,"FORMATO INCORRECTO DE CORREO",
                            Toast.LENGTH_LONG).show();
                }else if(validaPass(contrasena) == false){
                    Toast.makeText(MainActivity.this,"LA CONTRASEÑA DEBE TENER MINIMO " +
                                    "UNA LETRA MAYUSCULA, MINIMO UNA MINUSCULA, MINIMO UN NUMERO " +
                                    "Y TENER UN MINIMO DE LONGITUD DE 8",
                            Toast.LENGTH_LONG).show();
                }else if(listaArrayUsuario.get(0).equals(usuario_1)){
                    if(listaArrayUsuario.get(1).equals(contrasena)){
                        Toast.makeText(MainActivity.this,"¡ES CORRECTO!",
                                Toast.LENGTH_LONG).show();
                        usuario_tarea = et_mail.getText().toString();
                        inicioSesion();
                    }else{
                        Toast.makeText(MainActivity.this,"Credenciales son invalidas",
                                Toast.LENGTH_LONG).show();
                    }
                }else if(listaArrayUsuario.get(0) != usuario_1 && validaCorreo(usuario_1)){
                    //INSERT
                    try {
                        Usuarios usuario = new Usuarios(usuario_1, contrasena);
                        db.ususariosDao().insert(usuario);
                        Toast.makeText(MainActivity.this, "REGISTRO EXITOSO", Toast.LENGTH_LONG).show();
                        //inicioSesion();
                    }catch (Exception e){
                        System.out.println(e);
                    }
                }
            }
        });
    }
    //metodo de limpieza de campos
    //private void limpiar() {
    //nombre del campo  .setText("");

    private void inicioSesion(){
        Intent intent = new Intent(this, TareaActivity.class);
        startActivity(intent);
    }
    private ArrayList<String> verificaUsuario(List<Usuarios> lista, String llave){
        ArrayList<String> listaArrayUsuario = new ArrayList<>();
        String sinRespuesta = null;
        for(int i = 0; i<lista.size();i++){
            if(lista.get(i).usuario.equals(llave)){
                listaArrayUsuario.add(0,lista.get(i).usuario);
                listaArrayUsuario.add(1,lista.get(i).password);
                return listaArrayUsuario;
            }
        }
        listaArrayUsuario.add(0,"sinRespuesta");
        listaArrayUsuario.add(1,"sinRespuesta");
        return listaArrayUsuario;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("¿Desea salir de Lista de tareas?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        }

        return super.onKeyDown(keyCode, event);
    }

    public boolean validaCorreo(String correo){
        //Regular Expression
        String regx = "^[A-Za-z0-9+_.-]+@(.+)$";
        //Compile regular expression to get the pattern
        Pattern pattern = Pattern.compile(regx);
        Matcher matcher = pattern.matcher(correo);
        System.out.println(correo +" : "+ matcher.matches()+"\n");
        return matcher.matches();
    }
    public boolean validaPass(String pass){
        //1 mayuscula, 1 minuscula, 1 numero minimo
        char clave;
        byte  contNumero = 0, contLetraMay = 0, contLetraMin=0, contChar = 0;
        if(pass.length() < 8){
            return false;
        }
        for (byte i = 0; i < pass.length(); i++) {
            clave = pass.charAt(i);
            String passValue = String.valueOf(clave);
            if (passValue.matches("[A-Z]")) {
                contLetraMay++;
            } else if (passValue.matches("[a-z]")) {
                contLetraMin++;
            } else if (passValue.matches("[0-9]")) {
                contNumero++;
            }
        }
        if(contNumero > 0 && contLetraMin > 0 && contLetraMay > 0){
            return true;
        }else{
            return false;
        }
    }
}