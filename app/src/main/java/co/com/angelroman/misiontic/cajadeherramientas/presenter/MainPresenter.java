package co.com.angelroman.misiontic.cajadeherramientas.presenter;

import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.angelroman.misiontic.cajadeherramientas.model.MainInteractor;
import co.com.angelroman.misiontic.cajadeherramientas.mvp.MainMVP;
import co.com.angelroman.misiontic.cajadeherramientas.view.AppDataBase;
import co.com.angelroman.misiontic.cajadeherramientas.view.ListaTareas;
import co.com.angelroman.misiontic.cajadeherramientas.view.MainActivity;
import co.com.angelroman.misiontic.cajadeherramientas.view.TareaActivity;
import co.com.angelroman.misiontic.cajadeherramientas.view.dto.TaskItem;
import co.com.angelroman.misiontic.cajadeherramientas.view.dto.TaskState;


public class MainPresenter extends AppCompatActivity implements MainMVP.Presenter {

    private final MainMVP.View view;
    private final MainMVP.Model model;
    private final AppDataBase db;

    public MainPresenter(MainMVP.View view, String user, AppDataBase db) {
        this.view = view;
        this.model = new MainInteractor(db, user);
        this.db = db;
    }

    @Override
    public void loadTasks() {
        List<TaskItem> items = model.getTasks();

        view.showTaskList(items);
    }

    @Override
    public void addNewTask(String user) {
        Log.i(MainPresenter.class.getSimpleName(), "Add new Task");
        String description = view.getTaskDescription();
        String date = SimpleDateFormat.getDateTimeInstance().format(new Date());
        List<ListaTareas> user_1 = db.listaTareasDao().findByNameTask(user, description);

        ListaTareas tarea = new ListaTareas(user, description, "N");
        tarea.setFecha(date);
        try {
            db.listaTareasDao().insert(tarea);
            System.out.println("SE INSERTOOOOOOOOOOO TAREA = "+tarea.getNom_tarea());
        }catch (Exception e){
            System.out.println(""+e);
        }

        TaskItem task = new TaskItem(description, date, TaskState.PENDING);
        model.saveTask(task);
        view.addTaskToList(task);
    }

    @Override
    public void taskItemClicked(TaskItem task) {
        String message = task.getState() == TaskState.PENDING
                ? "Would you like to mark as DONE this task?"
                : "Would you like to mark as PENDING this task?";
        view.showConfirmDialog(message, task);
    }

    @Override
    public void updateTask(TaskItem task, String user) {
        List<ListaTareas> user_1 = db.listaTareasDao().findByNameTask(user, task.getDescription());
        task.setState(task.getState() == TaskState.PENDING ? TaskState.DONE : TaskState.PENDING);
        String description = view.getTaskDescription();
        if(task.getState().equals(TaskState.DONE)){
            String state = "S";
            System.out.println("MAINPRESENTER ESTADO S-----------> "+state);
            update(state,user_1.get(0).getId_tarea());
        }else if(task.getState().equals(TaskState.PENDING)){
            String state = "N";
            System.out.println("MAINPRESENTER ESTADO N-----------> "+state);
            update(state,user_1.get(0).getId_tarea());
        }else{
            System.out.println("MAINPRESENTER UPDATE TASK PROBLEMA");
        }
        model.updateTask(task);
        view.updateTask(task);
    }

    @Override
    public void taskItemLongClicked(TaskItem task) {
        if (task.getState() == TaskState.DONE) {
            view.showDeleteDialog("Would you like to REMOVE this task?", task);
        }
    }

    @Override
    public void deleteTask(TaskItem task, String user) {
        List<ListaTareas> user_1 = db.listaTareasDao().findByNameTask(user, task.getDescription());
        try {
            int delet = db.listaTareasDao().deleteTask(user_1.get(0).getId_tarea());
            if(delet > 0){
                Toast.makeText(MainPresenter.this, "TAREA ELIMINADA", Toast.LENGTH_LONG).show();
                System.out.println("TAREA ELIMINADA "+ task.getDescription());
            }
        }catch (Exception e){
            System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"+e);
        }
        model.deleteTask(task);
        view.deleteTask(task);
    }
    public void update(String state, int id){
        try {
            db.listaTareasDao().updateTask(state,id);

            System.out.println("TAREA = x CAMBIADA A ESTADO = "+state+" ------------------->sdsfsdfsd");

        }catch (Exception e){
            System.out.println("AAAAAAAAAAAAAAA"+e);
        }
    }
}
