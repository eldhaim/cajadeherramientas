package co.com.angelroman.misiontic.cajadeherramientas.view;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tarea")
public class ListaTareas {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id_tarea")
    public int id_tarea;
    @ColumnInfo(name = "usuario")
    public String usuario;
    @ColumnInfo(name = "nom_tarea")
    public String nom_tarea;
    @ColumnInfo(name = "fecha")
    public String fecha;
    @ColumnInfo(name = "estado")
    public String estado;


    public ListaTareas(String usuario, String nom_tarea, String estado) {
        this.usuario = usuario;
        this.nom_tarea = nom_tarea;
        this.estado = estado;
    }

    public int getId_tarea() {
        return id_tarea;
    }

    public void setId_tarea(int id_tarea) {
        this.id_tarea = id_tarea;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNom_tarea() {
        return nom_tarea;
    }

    public void setNom_tarea(String nom_tarea) {
        this.nom_tarea = nom_tarea;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
